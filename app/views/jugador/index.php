<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>

<body>

  <?php require "../app/views/parts/header.php" ?>


<table  class="table table-striped">

  <tr>
    <td>id:</td>
    <td>nombe:</td>
    <td>puesto:</td>
    <td>fecha de nacimiento:</td>
    <td>operaciones:</td>

  </tr>

     <?php foreach ($jugador as $player): ?>
        <tr>
            <td><?php echo $player->id ?></td>
            <td><?php echo $player->nombre ?></td>
            <td><?php echo $player->nacimiento ?></td>
        </tr>

    <?php endforeach ?>

</table>

  <hr>
  <a href="/jugador/create">nuevo</a>
  <?php require "../app/views/parts/footer.php" ?>


</body>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<?php require "../app/views/parts/scripts.php" ?>
</html>

