<?php

namespace App\Models;

//use PDO;
use Core\Model;

require_once '../core/Model.php';

/**
*
*/
class Jugador extends Model
{

    function __construct()
    {

    }//constructor

      public function __get($atributoDesconocido)
    {
        if (method_exists($this, $atributoDesconocido)) {
            $this->$atributoDesconocido = $this->$atributoDesconocido();
            return $this->$atributoDesconocido;
            // echo "<hr> atributo $x <hr>";
        } else {
            return "";
        }
    }

    public static function allplayers()
    {
        $db=Jugador::db();
        $statement = $db->query('SELECT * FROM jugadores');
        $jugador = $statement->fetchAll(PDO::FETCH_CLASS,Jugador::class);

        return $jugador;
    }

    public static function findplayer()
    {
        $db=Jugador::db();
        $statement = $db->query('SELECT * FROM jugadores WHERE id=:id');
        $stmt->execute(array(':id'=>$id));
        $stmt->setFetchMode(PDO::FETCH_CLASS,Jugador::class);
        $jugador = $statement->fetchAll(PDO::FETCH_CLASS,Jugador::class);

        return $jugador;
    }

    public function insertPlayer(){
        $db=Jugador::db();
        $stmt = $db->prepare("INSERT INTO jugadores(nombre,nacimiento, id_puesto)VALUES (:nombre,:nacimiento, :id_puesto)");

        $stmt->bindValue(':nombre', $this->nombre);
        $stmt->bindValue(':nacimiento', $this->nacimiento);
        $stmt->bindValue(':id_puesto', $this->id_puesto);
        return $stmt->execute();
    }

     public static function paginate($size = 10){

        if(isset($_REQUEST["page"])){
            $page = (integer) $_REQUEST["page"];
        }else{
            $page = 1;
        }

        $offset = ($page - 1) * $size;

        $db = Jugador::db();

        $statement = $db->prepare('SELECT * FROM jugadores LIMIT :pagesize OFFSET :offset');
        $statement->bindValue(":pagesize", $size, PDO::PARAM_INT);
        $statement->bindValue(":offset", $offset, PDO::PARAM_INT);
        $statement->execute();
        $jugador = $statement->fetchAll(PDO::FETCH_CLASS,Jugador::class);
        return $jugador;
    }


}//class
